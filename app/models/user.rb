class User < ActiveRecord::Base
  has_one :profile, :dependent => :destroy
  after_create :my_profile
  validates :email, presence: false, uniqueness: true, format: {with:Devise.email_regexp, allow_blank: true}
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  private
  def my_profile
    Profile.create(user_id: self.id)
  end
end
