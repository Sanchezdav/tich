class Profile < ActiveRecord::Base
  belongs_to :user
  before_create :create_alias
  validates :alias, uniqueness: { message: "El alias ya ha sido tomado, modifíquelo por favor!" }, allow_blank: true
  validates :cellphone, length: { minimum: 10, maximum: 10 }, allow_blank: true

  private
  def create_alias
    self.alias = SecureRandom.hex(10)
  end
end
