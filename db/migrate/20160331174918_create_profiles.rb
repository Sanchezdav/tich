class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true, foreign_key: true, on_delete: :cascade
      t.string :name
      t.string :last
      t.string :avatar
      t.text :bio
      t.string :alias
      t.string :country
      t.string :cellphone

      t.timestamps null: false
    end
  end
end
